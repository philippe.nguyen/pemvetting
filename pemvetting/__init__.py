OMEGA_PIPELINE = '/home/omega/opt/omega/bin/wpipeline'
OMEGA_GENERATOR = '/home/detchar/etc/ligo-channel-lists/tools/clf-to-omegascan.py'
FRAME_PATH = '/hdfs/frames/O2'
CMAP = 'viridis'

COUP_FUNC_DIR = '/home/philippe.nguyen/public_html/test/pemcoupling_test/CoupFuncData'
FALSE_RATES = ['5e-3'] #['1e-6', '1e-5', '1e-4', '1e-3', '5e-3']