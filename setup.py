from setuptools import setup, find_packages

setup(
    name='pemvetting',
    version='0.0',
    url='http://pem.ligo.org',
    author='Philippe Nguyen',
    author_email='philippe.nguyen@ligo.org',
    description='PEM vetting of gravitational wave candidates',
    license='GNU General Public License Version 3',
    packages=find_packages(),
    scripts=[
        'bin/pemvetting'
    ]
)