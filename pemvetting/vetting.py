#! /usr/bin/env python

import numpy as np
import pandas as pd
import os

from gwpy.detector import Channel

# pemvetting modules
from utils import (vetting_results, spec_plot)
from event import Event
from amplitudes import check_amplitudes
from overlaps import check_overlaps

def vetting(graceid, ifo, config_strain, config_full, verbose=False):
    """
    Perform PEM vetting of GW event candidate from GraceDb id number.
    
    Parameters
    ----------
    graceid : str
        GraceDb id for the event.
    
    Returns
    -------
    state : str
        'pass' or 'hin' for human input needed.
    """
    
    # Directories
    results_dir = os.path.join(os.getcwd(), graceid, ifo)
    omega_dir = os.path.join(results_dir, 'omegascans')
    failed_channels_filename = os.path.join(results_dir, 'failed_channels.txt')
    plots_dir = os.path.join(results_dir, 'vettingplots')
    # Create Event object from GraceDb info
    event = Event(graceid, ifo)
    
    #### COUPLING FUNCTION CHECKS ####
    amplitude_results, failed = check_amplitudes(
        event, omega_dir, config_strain, config_full, verbose=verbose)
    if verbose:
        print('PEM amplitude checks for %s complete.' % ifo)
    channels = sorted(amplitude_results.keys())
    failed_dict = {channel: 'NoCouplingFunctionFound' for channel in failed}
    
    #### SIGNAL OVERLAP CHECKS ####
    overlap_results = check_overlaps(event, channels, verbose=verbose)
    if verbose:
        print('Time-frequency overlap checks for %s complete.' % ifo)
    contour_overlap_results = overlap_results['contour_overlap']
    path_overlap_results = overlap_results['path_overlap']
    specs = overlap_results['spectrograms']
    for channel in overlap_results['failed_channels']:
        failed_dict[channel] = 'ChannelFailedToLoad'
        
    #### SAVE RESULTS ####
    # Report channels with no composite coupling functions found
    with open(failed_channels_filename, 'wb') as file:
        failed_channels = sorted(failed_dict.keys())
        lines = []
        for channel in failed_channels:
            lines.append('{:<45}{}'.format(channel, failed_dict[channel]))
        file.write('\n'.join(lines))
        if len(failed_channels) > 0 and verbose:
            print('Failed channels:')
            for c in failed_channels:
                print(c)
    if not os.path.exists(results_dir):
        os.makedirs(results_dir)
    # State: 'pass', 'fail', or 'hin' (human input needed)
    results_filename = os.path.join(results_dir , 'vetting_results.txt')
    state = vetting_results(
        results_filename, amplitude_results,
        contour_overlap_results, path_overlap_results, verbose=verbose)
    if not os.path.exists(plots_dir):
        os.makedirs(plots_dir)
    outseg_t = (event.outseg[1] - event.outseg[0])
    plot_delta_t = min([event.plot_times[0], outseg_t / 2.])
    tplot = (event.end_time - plot_delta_t + 0.01, event.end_time + 0.01)
    for spec in specs:
        plot_filename = os.path.join(
            plots_dir, '%s.png' % spec.name.rstrip('_DQ'))
        spec_plot(spec, plot_filename, tplot=tplot, verbose=verbose)
    return state