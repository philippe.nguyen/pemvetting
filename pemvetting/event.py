#! /usr/bin/env python

import numpy as np
import os
import subprocess
import warnings
import re
from copy import copy

from gwpy.detector import Channel
from gwpy.timeseries import TimeSeries
from pycbc.waveform import get_td_waveform, frequency_from_polarizations
from lalsimulation import SIM_INSPIRAL_FRAME_AXIS_ORBITAL_L

# pemvetting modules
import utils as vettingutils
from . import (OMEGA_PIPELINE, OMEGA_GENERATOR, FRAME_PATH, CMAP)

class Event(Channel):
    """
    A single GW event candidate at a single interferometer.
    
    Attributes
    ----------
    name : str
        Name of GW strain channel.
    ifo : str
        Interferometer
    graceid : str
        GraceDb id number.
    m1 : float
        Component mass 1.
    m2 : float
        Component mass 2.
    s1z : float
        Component z-direction spin 1.
    s2z : float
        Component z-direction spin 2.
    start_time : float
        GraceDb template start time.
    end_time : float
        GraceDb template end time.
    template_duration : float
        GraceDb template duration.
    tf_path : gwpy.timeseries.TimeSeries object
        Time-frequency path of candidate.
    frametype : str
        Frame type for extracting data.
    sample_rate : int
        Sample rate (Hz) of strain channel for omega scans.
    time_range : int
        Time range (seconds) for omega scan data extraction.
    search_frequency_range : tuple
        Min and max frequencies for omega scans.
    search_q_range : tuple
        Min and max Q values for omega scans.
    window_duration : int, float
        Time window (seconds) for omega scan search.
    plot_times : array-like
        Time windows (seconds) for omega scan plots.
    overlap_t0 : float
        Start time used in overlap checks.
    overlap_t1 : float
        End time used in overlap checks.
    outseg : tuple
        Start and end time of spectrograms used in overlap checks.
    tres : float
        Time resolution of spectrograms used in overlap checks.
    fres : float
        Frequency resolution of spectrograms used in overlap checks.
    """
    
    def __init__(self, graceid, ifo):
        self._init_event_attrs(graceid, ifo)
        # Time-frequency path (TimeSeries object)
        self.tf_path = self._get_tf_path()
        # Omega scan parameters
        self._init_scan_attrs(ifo)
        # Parameters to be used in overlap checks
        delta_t_ = 64 # Default time range for the omega scans
        self.t0 = self.scan_time - delta_t_ / 2.
        self.t1 = self.scan_time + delta_t_ / 2.
        self.outseg = (self.end_time - delta_t_ / 50. + 0.1,
                       self.end_time + 0.1)
        self.tres = delta_t_ / 1e5
        self.fres = 1
    
    def _init_event_attrs(self, graceid, ifo):
        """
        Initialize event attributes from GraceDb page.
        """
        event_dict = vettingutils.get_event(graceid)[ifo]
        for attr, value in event_dict.items():
            setattr(self, attr, value)

    def _init_scan_attrs(self, ifo):
        """
        Initialize attributes containing omega scan parameters.
        """
        if self.tf_path is None:
            window_duration = vettingutils.estimate_duration(self.mchirp)
        else:
            window_duration = 1.25 * self.tf_path.duration.value
        if window_duration > 10:
            time_range = 2048
            qlow = 4
            qhigh = 150
        else:
            time_range = 64
            qlow = 4
            qhigh = 96
        plot_times = np.logspace(
            np.log10(window_duration),
            np.log10(self.template_duration), 4)[:-1]
        scan_params = {
            'frametype'       : '{}_HOFT_C00'.format(ifo),
            'sample_rate'     : 16384,
            'time_range'      : time_range,
            'qlow'            : qlow,
            'qhigh'           : qhigh,
            'window_duration' : window_duration,
            'plot_times'      : plot_times,
        }
        self.scan_time = 0.5 * (self.end_time + self.tf_path.times.value.min())
        super(Event, self).__init__(ifo + ':GDS-CALIB_STRAIN', **scan_params)
    
    def _get_tf_path(self, approximant='SEOBNRv4_opt', sample_rate=4096*4):
        """
        Get frequency track of an event based on GraceDb parameters.

        Parameters
        ----------
        approximant : str, optional
        f_lower : int, optional
        sample_rate : int, optional

        Returns
        -------
        tf_path : gwpy.timeseries.TimeSeries object
            Time series of frequency.
        """
        if not all(hasattr(self, x) for x in 
                   ['graceid', 'm1', 'm2', 's1z', 's2z',
                    'start_time', 'end_time']):
            return None
        hp, hc = get_td_waveform(
            approximant='SEOBNRv4_opt',
            mass1=self.m1,
            mass2=self.m2,
            spin1z=self.s1z,
            spin2z=self.s2z,
            delta_t=1.0/sample_rate,
            f_lower=30,
            frame_axis=SIM_INSPIRAL_FRAME_AXIS_ORBITAL_L
        )
        foft = frequency_from_polarizations(hp, hc)
        try:
            foft = vettingutils.trim_foft(foft)
        except:
            pass
        tf_path = TimeSeries(
            foft, t0=self.end_time+float(foft.start_time), dt=foft.delta_t)
        t0 = max([tf_path.times.value.min(), self.start_time])
        t1 = min([tf_path.times.value.max(), self.end_time])
        tf_path = tf_path.crop(t0, t1)
        tf_path.name = self.graceid + '_path'
        return tf_path

    def find_lowest_false_rate(self, false_alarm_rates,
                               omega_scan_dir, config_template):
        """
        Determine the lowest false alarm rate that triggers a strain omega scan.

        Parameters
        ----------
        false_alarm_rates : array-like
            False alarm rate values to run omega scans with.

        Returns
        -------
        output : str
            Lowest false rate that triggered a strain omega scan.
        """
        # self.wnfar = '5e-3'
        # return
        if not os.path.exists(omega_scan_dir):
            os.makedirs(omega_scan_dir)
        cfg = OmegaScanConfig.from_file(config_template)
        sections = cfg.sections
        strain_channel = sections['Calibrated h(t)'][0]
        cfg_new = []
        for far in false_alarm_rates:
            replace_mapping = {
                'searchTimeRange'      : self.time_range,
                'searchQRange'         : [self.qlow, self.qhigh],
                'whiteNoiseFalseRate'  : far,
                'searchWindowDuration' : self.window_duration,
                'plotTimeRanges'       : list(self.plot_times)
            }
            new_channel = strain_channel.replace(replace_mapping)
            cfg_new.append(new_channel)
        sections['Calibrated h(t)'] = cfg_new
        # Save new config file
        filename = os.path.join(omega_scan_dir, 'configuration.txt')
        cfg.save(filename)
        # Run strain omega scans
        cfg.run(self.scan_time, filename, omega_scan_dir)
        strain_scan_summary = cfg.summary_file
        strain_scan = vettingutils.read_omega_scan_summary(strain_scan_summary)
        # Determine lowest false alarm rate
        strain_scan['falseRate'] = false_alarm_rates
        triggered_scans = strain_scan[strain_scan['peakTime'].astype(int) > 0]
        if triggered_scans.shape[0] > 0:
            trig_far = triggered_scans['falseRate']
            lowest_far = trig_far[trig_far.astype(float).values.argmin()]
            print('Lowest false alarm rate for %s: %s'
                  % (self.ifo, lowest_far))
        else:
            lowest_far = '1e-3'
            print('Could not trigger CALIB_STRAIN omega scan. ' +
                  'Choosing default false rate of 1e-3.')
        self.wnfar = lowest_far
        return
    
    def create_config(self, config_filename, chanlist_filename,
                      omega_generator=OMEGA_GENERATOR):
        """
        Create configuration file for a full omega scan using
        DetChar's clf-to-omegascan.py tool.
        
        Parameters
        ----------
        config_filename : str
            Configuration file name.
        chanlist_filename : str
            Channel list .ini file for generating final config file.
        omega_generator : str
            Script for generating config file.
        """
        
        cmd = (
            '{command} {chanlist} -o {output} ' +
            '--search-time-range {time_range} ' +
            '--q-low {qlow} --q-high {qhigh} ' +
            '--white-noise-false-alarm-rate {wnfar} ' +
            '--search-window-duration {window_duration} ' +
            '--plot-time-range {plot0} ' +
            '--plot-time-range {plot1} ' +
            '--plot-time-range {plot2} '
        ).format(**{
            'command'         : omega_generator,
            'chanlist'        : chanlist_filename,
            'output'          : config_filename,
            'time_range'      : self.time_range,
            'qlow'            : self.qlow,
            'qhigh'           : self.qhigh,
            'wnfar'           : self.wnfar,
            'window_duration' : self.window_duration,
            'plot0'           : self.plot_times[0],
            'plot1'           : self.plot_times[1],
            'plot2'           : self.plot_times[2]
        })
        subprocess.call(cmd, shell=True)

class OmegaScanChannel(object):
    """
    Contains omega scan config options for a single channel.
    """
    
    def __init__(self, name, **params):
        self.channelName = name
        for key, value in params.items():
            setattr(self, key, value)
    
    @classmethod
    def from_string(cls, s):
        """
        Parse an omega scan entry for config options.
        
        Paramters
        ---------
        s : str
            Omega scan entry
        
        Returns
        -------
        new : OmegaScanChannel object
            Config options for a single channel.
        """
        
        patterns = [
            ('channelName', "'([A-Z0-9:\-_]+)'"),
            ('frameType', "'([A-Z0-9:\-_]+)'"),
            ('sampleFrequency', "([0-9]+)"),
            ('searchTimeRange', "([0-9]+)"),
            ('searchFrequencyRange', "\[([0-9]+)?[ ]?([0-9]+)\]"),
            ('searchQRange', "\[([0-9]+)?[ ]?([0-9]+)\]"),
            ('searchMaximumEnergyLoss', "([0-9.]+)"),
            ('whiteNoiseFalseRate', "([0-9]+e\-[0-9]+)"),
            ('searchWindowDuration', "([0-9.]+)"),
            ('plotTimeRanges', "\[([0-9.]+)?[ ]?([0-9.]+)?[ ]?([0-9.]+)\]"),
            ('plotFrequencyRange', "\[([0-9.]+)?[ ]?([0-9.]+)\]"),
            ('plotNormalizedEnergyRange', "\[([0-9.]+)?[ ]?([0-9.]+)\]"),
            ('alwaysPlotFlag', "([0-9]?)")
        ]
        str_attrs = ['channelName', 'frameType', 'whiteNoiseFalseRate']
        params = {}
        for attr, pattern in patterns:
            template = "[ ]+%s:[ ]+%s" % (attr, pattern)
            match = re.search(template, s)
            if match is None:
                if attr in str_attrs:
                    value = ''
                else:
                    value = None
            elif len(match.groups()) > 1:
                value = list(match.groups())
            else:
                value = match.group(1)
            if type(value) == list:
                try:
                    value_new = [int(x) for x in value]
                except:
                    try:
                        value_new = [float(x) for x in value]
                    except:
                        value_new = value
            else:
                try:
                    value_new = int(value)
                except:
                    if attr != 'whiteNoiseFalseRate':
                        try:
                            value_new = float(value)
                        except:
                            value_new = value
            params[attr] = value_new
        name = params.pop('channelName')
        new = OmegaScanChannel(name, **params)
        return new
    
    def to_string(self):
        """
        Convert attributes to a formatted omega scan entry.
        
        Returns
        -------
        s : str
            Formatted string to be appended to an omega scan.
        """
        
        replace_dict = self.__dict__
        str_attrs = ['channelName', 'frameType', 'whiteNoiseFalseRate']
        lst_attrs = [
            'searchFrequencyRange', 'searchQRange',
            'plotFrequencyRange', 'plotNormalizedEnergyRange']
        for key, value in replace_dict.items():
            if key in str_attrs and value is None:
                replace_dict[key] = ''
            elif key == 'plotTimeRanges':
                replace_dict[key] = (
                    '[%.1f %.1f %.1f]' % tuple(value))
            elif key in lst_attrs:
                if value is None:
                    replace_dict[key] = []
                else:
                    replace_dict[key] = (
                        '[%.1f %.1f]' % tuple(value))
        template = ("{{\n" +
            "  channelName:               '{channelName}'\n" +
            "  frameType:                 '{frameType}'\n" +
            "  sampleFrequency:           {sampleFrequency}\n" +
            "  searchTimeRange:           {searchTimeRange}\n" +
            "  searchFrequencyRange:      {searchFrequencyRange}\n" +
            "  searchQRange:              {searchQRange}\n" +
            "  searchMaximumEnergyLoss:   {searchMaximumEnergyLoss}\n" +
            "  whiteNoiseFalseRate:       {whiteNoiseFalseRate}\n" +
            "  searchWindowDuration:      {searchWindowDuration}\n" +
            "  plotTimeRanges:            {plotTimeRanges}\n" +
            "  plotFrequencyRange:        {plotFrequencyRange}\n" +
            "  plotNormalizedEnergyRange: {plotNormalizedEnergyRange}\n" +
            "  alwaysPlotFlag:            {alwaysPlotFlag}\n" +
            "}}\n")
        s = template.format(**replace_dict)
        return s
    
    def replace(self, mapping):
        new = copy(self)
        for key, value in mapping.items():
            setattr(new, key, value)
        return new

class OmegaScanConfig(OmegaScanChannel):
    """
    Config parser for omega scans.
    """
    
    def __init__(self, filename, sections):
        self.filename = filename
        self.sections = sections
    
    @classmethod
    def from_file(cls, filename):
        """
        Parse omega scan file for config options.
        
        Parameters
        ----------
        filename : str
            Omega scan config file.
        
        Returns
        -------
        new : OmegaScanConfig object
            Contains config options separated by channel and section.
        """
        
        with open(filename) as f:
            lines = f.readlines()
        sections = {}
        i = 0
        while i < len(lines):
            if lines[i][0] == '[':
                sec = re.findall('\[(.*),.*\]', lines[i])[0]
                sections[sec] = []
            if lines[i].replace(' ','') == '{\n':
                j = i
                while j < len(lines):
                    if '}' in lines[j]:
                        entry = ''.join(lines[i:j+1])
                        sections[sec].append(entry)
                        break
                    else:
                        j += 1
                i = j
            else:
                i += 1
        out = {}
        for section, channels in sections.items():
            out[section] = [
                super(OmegaScanConfig, cls).from_string(channel)
                for channel in channels]
        new = OmegaScanConfig(filename, out)
        return new
    
    def save(self, file_out):
        """
        Save omega scan config options to a config file.
        
        Parameters
        ----------
        file_out : str
            output config filename.
        """
        
        with open(self.filename, 'r') as f:
            lines = f.readlines()
        i = 0
        while i < len(lines):
            if lines[i][0] == '[':
                break
            else:
                i += 1
        new = ''#.join(lines[:i])
        section_names = sorted(self.sections.keys())
        if 'Context' in section_names:
            sec = 'Context'
            section_names.insert(
                0, section_names.pop(section_names.index(sec)))
        for sec in section_names:
            new += '\n[%s, %s]\n' % (sec, sec)
            entries = self.sections[sec]
            for entry in entries:
                new += '\n' + entry.to_string()
        with open(file_out, 'w') as f:
            f.write(new)
        return
    
    def run(self, gpstime, config, out_dir, omega_pipeline=OMEGA_PIPELINE,
            frame_path=FRAME_PATH, cmap=CMAP):
        """
        Run this omega scan via the wpipeline.

        Parameters
        ----------
        gpstime : float
            Scan time.
        config : str
            Configuration file.
        out_dir : str
            Output directory.
        """
        
        out_dir = os.path.join(out_dir, '')
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)
        if os.path.exists(os.path.join(out_dir, 'lock.txt')):
            subprocess.call('rm ' + out_dir + 'lock.txt', shell=True)
        cmd_split = [
            OMEGA_PIPELINE + ' scan',
            str(gpstime),
            '-o', out_dir,
            '-c', config,
            '-f', frame_path,
            '-m', cmap,
            '-r'
        ]
        cmd = ' '.join(cmd_split)
        print(cmd)
        subprocess.call(cmd, shell=True)
        self.summary_file = os.path.join(out_dir, 'summary.txt')
        return